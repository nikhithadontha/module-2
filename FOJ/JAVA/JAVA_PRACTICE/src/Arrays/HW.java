package Arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class Student {
    private static int studentCount = 100;
    private int id;
    private String name;
    private List<Integer> marks;

    public Student(String name, List<Integer> marks) {
        this.id = ++studentCount;
        this.name = name;
        this.marks = marks;
    }

    public int calculateTotal() {
        int total = 0;
        for (int mark : marks) {
            total += mark;
        }
        return total;
    }

    public double calculateAverage() {
        return (double) calculateTotal() / marks.size();
    }

    public String calculateResult() {
        for (int mark : marks) {
            if (mark < 35) {
                return "Fail";
            }
        }
        return "Pass";
    }

    public String calculateDivision() {
        double average = calculateAverage();
        if (average >= 60) {
            return "I Division";
        } else if (average >= 50) {
            return "II Division";
        } else if (average >= 40) {
            return "III Division";
        } else {
            return "Fail";
        }
    }

    public String generateResultRow(String result, String division) {
        int total = calculateTotal();
        double average = calculateAverage();
        return String.format("%d %s %d %d %d %d %.2f %s %s", id, name, marks.get(0), marks.get(1), marks.get(2), total, average, result, division);
    }
}

public class HW {
    public static void main(String[] args) {
        List<Student> students = new ArrayList<>();
        students.add(new Student("Harsha", Arrays.asList (11, 22, 33)));d
        students.add(new Student("Pasha", Arrays.asList  (22, 33, 44)));
        students.add(new Student("Krishna", Arrays.asList(44, 15, 10)));

        System.out.println("Sid Sname S1 S2 S3 Tot   Avg Result Division");
        System.out.println("------------------------------------------");

        for (Student student : students) {
            String result = student.calculateResult();
            String division = student.calculateDivision();
            System.out.println(student.generateResultRow(result, division));
        }
    }
}
