package JAVA;

public class Adam_Number {

	public static int reverse (int num){

		int rev = 0;
		while(num > 0){
			rev = rev * 10 + num % 10;
			num = num / 10;
		}
		return rev;
	}


	public static boolean isAdam(int num){

		int square = num * num;
		int revSquare = reverse(square);
		int revNum = reverse(num);
		int squareOfRevNum = revNum * revNum;
		return revSquare == squareOfRevNum;

	}
	public static void main(String[] args) {
		int number = 22324;

		if(isAdam(number)){
			System.out.println(number+ " is  an Adam's number. ");

		}else{
			System.out.println(number + " is not an Adam's number.");

		}
	}

}
