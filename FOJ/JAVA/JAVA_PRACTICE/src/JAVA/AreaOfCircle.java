package JAVA;


public class AreaOfCircle {

	public static void main(String[] args) {

		double radius = 3;
		double Circlearea = Math.PI*radius*radius;
		// TODO Auto-generated method stub

		String area = String.format("%.2f", Circlearea);

		System.out.println("The area of a circle with radius " + radius + " is : " + area);

	}
}

