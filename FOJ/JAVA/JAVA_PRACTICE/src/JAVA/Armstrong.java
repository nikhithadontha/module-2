package JAVA;

public class Armstrong {

	public static boolean isArmStrong(int number){
		int initialNum = number;
		int sum = 0;
		int pow = 0;
		int power = number;

		while(power != 0){
			power = power / 10;
			pow++;
		}
		while(number !=0){
			int digit = number % 10;
			sum += Math.pow(digit, pow);
			number = number / 10;

		}
		return sum == initialNum;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int number = 1987873;
		if(isArmStrong(number)){
			System.out.println(number + " is an Armstrong number");
		}else{
			System.out.println(number + " is not an Armstrong number");
		}


	}

}
