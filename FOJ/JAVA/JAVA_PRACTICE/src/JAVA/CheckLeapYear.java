package JAVA;

public class CheckLeapYear {

	public static boolean isLeapYear(int year){
		if((year % 400 == 0) || (year % 100 !=0 && year % 4 == 0))
			return true;

		return false;
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isLeapYear(1994)); 
		System.out.println(isLeapYear(2002));  
		System.out.println(isLeapYear(2007));  
		System.out.println(isLeapYear(2000));  
		System.out.println(isLeapYear(2021));  
		System.out.println(isLeapYear(1996));  
		System.out.println(isLeapYear(2020));


	}

}
