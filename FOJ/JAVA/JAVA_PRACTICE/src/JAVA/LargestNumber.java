package JAVA;

public class LargestNumber {

    public static int largestOfTwo(int num1, int num2){
        if(num1 > num2){
            return num1;
        } else {
            return num2;
        }
    }

    public static int largestOfThree(int val1, int val2, int val3){
        if(val1 > val2 && val1 > val3){
            return val1;
        } else if(val2 > val1 && val2 > val3){
            return val2;
        } else {
            return val3;
        }
    }

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        System.out.println("Largest Of two: " + largestOfTwo(23, 89));
        System.out.println("Largest Of two: " + largestOfTwo(93, 39));
        
        System.out.println();

        System.out.println("Largest Of three: " + largestOfThree(90, 88, 65));
        System.out.println("Largest Of three: " + largestOfThree(99, 88, 55));
    }
}
