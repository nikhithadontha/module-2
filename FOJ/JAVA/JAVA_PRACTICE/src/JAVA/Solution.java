package JAVA;

public class Solution {

	boolean rev_sum_of_odd_val(int n) {
		int sum = 0;

		// Loop through each digit of the number
		while (n > 0) {
			int digit = n % 10;

			// Check if the digit is odd
			if (digit % 2 != 0) {
				sum += digit;
			}

			// Move to the next digit
			n = n / 10;
		}

		// Check if the sum of odd digits is even
		return sum % 2 == 0;
	}

	public static void main(String[] args) {
		Solution solution = new Solution();

		// Test cases
		System.out.println(solution.rev_sum_of_odd_val(2345)); // true
		System.out.println(solution.rev_sum_of_odd_val(57238)); // false
	}
}
