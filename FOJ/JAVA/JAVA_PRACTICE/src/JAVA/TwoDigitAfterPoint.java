package JAVA;

import java.text.DecimalFormat;

public class TwoDigitAfterPoint {

    // Method to round a double value to two decimal places
    public static void round2(double value) {
        double roundedValue = value * 100;
        roundedValue = (int) roundedValue;
        double result = roundedValue / 100;
        System.out.println("The rounded value for the 2 decimal values is : " + result);
    }

    // Method to round a double value using DecimalFormat
    public static void round2UsingDecimalFormat(double value) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        String rounded = decimalFormat.format(value);
        System.out.println("The rounded value using DecimalFormat is: " + rounded);
    }

    // Method to round a double value using String.format
    public static void round2UsingStringFormat(double value) {
        String rounded = String.format("%.2f", value);
        System.out.println("The rounded value using String.format is: " + rounded);
    }

    // Method to round a double value using Math.round
    public static void round2UsingMathRound(double value) {
        double rounded = Math.round(value * 100.0) / 100.0;
        System.out.println("The rounded value using Math.round is: " + rounded);
    }

    public static void main(String[] args) {
        double number = 12.3333333;
        round2(number);
        round2UsingDecimalFormat(number);
        round2UsingStringFormat(number);
        round2UsingMathRound(number);
    }
}
