package Patterns;

public class DiamondPattern {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int rows = 5; // Number of rows in the diamond

		// Upper part of the diamond
		for (int i = 0; i < rows; i++) {
			// Print spaces before each row
			for (int j = 0; j < rows - i - 1; j++) {
				System.out.print(" ");
			}

			// Print asterisks for each row
			for (int k = 0; k < 2 * i + 1; k++) {
				System.out.print("*");
			}

			// Move to the next line after each row
			System.out.println();
		}

		// Lower part of the diamond (mirrored upper part)
		for (int i = rows - 2; i >= 0; i--) {
			// Print spaces before each row
			for (int j = 0; j < rows - i - 1; j++) {
				System.out.print(" ");
			}

			// Print asterisks for each row
			for (int k = 0; k < 2 * i + 1; k++) {
				System.out.print("*");
			}

			// Move to the next line after each row
			System.out.println();
		}
	}
}



