package Patterns;

public class ReverseTriangle {

	public static void main(String[] args) {
		int rows = 5; // Number of rows in the reverse triangle

		for (int i = rows; i >= 1; i--) {
			// Print spaces before each row
			for (int j = 0; j < rows - i; j++) {
				System.out.print(" ");
			}

			// Print asterisks for each row
			for (int k = 1; k <= i; k++) {
				System.out.print("* ");
			}

			// Move to the next line after each row
			System.out.println();
		}
	}
}

