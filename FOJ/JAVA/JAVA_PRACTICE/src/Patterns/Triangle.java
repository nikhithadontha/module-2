package Patterns;

public class Triangle {

	public static void main(String[] args) {



		for (int i = 0; i < 5; i++) {
			// Print spaces before each row
			for (int j = 0; j < 5 - i - 1; j++) {
				System.out.print(" ");
			}

			// Print asterisks for each row
			for (int k = 0; k <= i; k++) {
				System.out.print("* ");
			}

			// Move to the next line after each row
			System.out.println();
		}


	}

}
