package WeeklyTests;

public class firstLast {

   public static boolean firstLast6(int[] nums) {
		int len = nums.length;
		return (nums[0] == 6 || nums[len - 1] == 6);
	}

	public static void main(String[] args) {
		
		System.out.println(firstLast6(new int[]{1, 2, 6})); 
		System.out.println(firstLast6(new int[]{6, 1, 2, 3})); 
		System.out.println(firstLast6(new int[]{13, 6, 1, 2, 3})); 
	}



}



