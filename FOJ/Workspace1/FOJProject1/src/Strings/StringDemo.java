package Strings;

public class StringDemo {

	public static void main(String[] args) {
		// create a string variable called name and
		// assign it with your full name
		String name = "Nikhitha Dontha";

		// Printing the total number of characters in the string
		System.out.println("name length: " + name.length());
		System.out.println("char at index 4: " + name.charAt(4));

		// print last character of the name without using
		// index position of the last character
		System.out.println("last character :" + name.charAt(name.length() - 1));

		// length()
		// charAt()

		// int indexOf(char ch) ->

		// print the index of first letter of second word

		/*
		 * 1. find the index of first space character
		 *  1.5. find the first character
		 * of the second word
		 *  2. use that index to find the index of first character
		 * of second word
		 */
		int indexOfSpace = name.indexOf(' ');
		char charAtIndex = name.charAt(indexOfSpace + 1);
		int indexOfSecondWord = name.indexOf(charAtIndex);
		System.out.println("first letter of second word: " + indexOfSecondWord);

		// print the first word of the name

		System.out.print(name.charAt(0));
		System.out.print(name.charAt(1));
		System.out.print(name.charAt(2));
		System.out.print(name.charAt(3));
		System.out.print(name.charAt(4));
		System.out.print(name.charAt(5));
		System.out.print(name.charAt(6));
		System.out.print(name.charAt(7));
		System.out.println();

		for(int i = 0; i < name.indexOf(' '); i++){
			System.out.print(name.charAt(i));
		}

		//        
		//        String firstWord = name.substring(0, indexOfSpace);
		//        System.out.println("First word of the name: " + firstWord);

		//assume your name contains only two words
		//print the both words in seperate lines 

		//print the reverse of the name
		String word = "Nikhitha";
		String reversedWord = "";
		//		String sur = "Dontha";
		//		String wordReverse = "";

		for (int i = word.length() - 1; i >= 0; i--) {
			reversedWord += word.charAt(i);
			//			for(int j = sur.length() -1; j >= 0; j--){
			//				wordReverse += sur.charAt(j);
			//			}
			//		}
			System.out.println();
			System.out.print(reversedWord);

			//        System.out.println();
			//		System.out.print(wordReverse);



			//print the reverse of each word by maintaining the 
			//order of the words 
			//ex:  "dontha nikhitha netha"
			//op:  "ahtnod ahtihkin ahten"



			// challenge : can you print all words in seperate lines, 
			//if there are more than 2 words in a name?


			// a method that takes plaintext and key
			//and returns cipher text
			String plainText = "VIKAS";

			System.out.println('a');
			System.out.println((int) 'a');

			System.out.println((char) 86);
			System.out.println((char) 73);
			System.out.println((char) 75);
			System.out.println((char) 65);
			System.out.println((char) 83);

		}
	}
}






