package day02;
 //Unary operator (++,--)
public class Demo3 {

	public static void main(String[] args) {
		int num = 10;
		System.out.println("num = "+ num + "\n");
		
		num++;
		System.out.println("num = "+ num + "\n");
		
		num--;
		System.out.println("num = "+ num + "\n");


	}

}
