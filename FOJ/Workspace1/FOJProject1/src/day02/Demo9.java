package day02;

public class Demo9 {

	public static void main(String[] args) {
		int num1= 10;
		int num2 = 20;
		int temp;
		
		System.out.println("Before swapping / Interchanging");
		System.out.println("num1 = "+ num1 + "\nnum2="+ num2 +"\n");
		
		temp = num1;
		num1 = num2;
		num2 = temp;
		
		
		System.out.println("After swapping / Interchanging");
		System.out.println("num1 = "+ num1 + "\nnum2="+ num2 +"\n");
		
		//swapping / Interchanging without using temp variable (approach-2)
		
		num1+= num2;
		num2 = num1-num2;
		
		
		System.out.println("After swapping / Interchanging");
		System.out.println("num1 = "+ num1 + "\nnum2="+ num2 +"\n");

	}

}
