package day03;

public class Demo4 {
	public static int sumOfTwoDigitsNum(int num ){
		return (num / 10) + (num % 10);
	}

	public static void main(String[] args) {
		System.out.println(sumOfTwoDigitsNum(23));
		System.out.println(sumOfTwoDigitsNum(25));
		System.out.println(sumOfTwoDigitsNum(45));
	}
}
