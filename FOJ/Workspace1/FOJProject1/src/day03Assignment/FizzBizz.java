package day03Assignment;

public class FizzBizz {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Define the number to check
		int number = 30; // You can change this number to any value you want to test

		// Check for divisibility and print the appropriate output
		if (number % 15 == 0) {
			System.out.println("FizzBizz");
		} else if (number % 3 == 0) {
			System.out.println("Fizz");
		} else if (number % 5 == 0) {
			System.out.println("Bizz");
		} else {
			System.out.println(number);
		}
	}


}


