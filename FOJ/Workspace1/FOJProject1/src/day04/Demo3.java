package day04;

public class Demo3 {
	//FizzBizz

	public static String isDivisibleBy3Or5(int num){
		String result = "";

		if(num % 3 == 0)
			result += "Fizz";

		if(num % 5 == 0)
			result += "Bizz";

		return result;


		//		if(num % 3 == 0 && num % 5 == 0){
		//			return "FizzBizz";
		//		}else if(num % 3 == 0){
		//			return "Fizz";
		//		}else if(num % 5 == 0){
		//			return "Bizz";
		//		}else{
		//			return "";
		//		}
	}
	public static void main(String[] args) {
		System.out.println(isDivisibleBy3Or5(30));
		System.out.println(isDivisibleBy3Or5(3));
		System.out.println(isDivisibleBy3Or5(5));
		System.out.println(isDivisibleBy3Or5(15));


	}

}
