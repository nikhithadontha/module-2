package day04;

public class Demo6 {



	public static void main(String[] args) {

		float distanceInMeters = 1000; 
		float timeInSeconds = 3600;     
		float speedInMetersPerSecond = distanceInMeters / timeInSeconds;
		float speedInKmph = (distanceInMeters / 1000.0f) / (timeInSeconds / 3600.0f);
		float speedInMph = speedInKmph / 1.609f;

		System.out.println("Speed in meters/second: " + speedInMetersPerSecond);
		System.out.println("Speed in kilometers/hour: " + speedInKmph);
		System.out.println("Speed in miles/hour: " + speedInMph);
	}
}