package day04Assignment;

public class SpeedInMeters2 {

	public static void main(String[] args) {

		float distance = 1000;
		float time = 3600;
		float speedinmeterpersec = distance/time;
		float speedinkmperhour = (distance/1000f)/(time/3600f);
		float speedinmilesperhour = (distance/1609f)/(time/3600f);

		System.out.println("speed in m/sec = " +  speedinmeterpersec);
		System.out.println("speed in km/hr = " +  speedinkmperhour);
		System.out.println("speed in miles/hr = " +  speedinmilesperhour);
	}

}
//Write a Java program that takes from user the distance (in meters) and the
//time  taken (as three numbers: hours, minutes, seconds), and display the speed,
//in meters per second, kilometers per hour and miles per hour (hint: 1 mile = 1609 meters).
