package day05;

public class Demo1 {

	public static void main(String[] args) {
		//while loop
		int num = 9;
		int i = 1;
		
		while(i < 11){
			
			System.out.println(num + " * " + i + " = " + (num * i));
			i++;
		}
	}

}
