package day05;

public class Demo8{
	public static boolean prime(int number){

		int n = 0;
		for(int i = 1; i <= number; i++){
			if(number % i ==0){
				n++;
			}
		}
		if(n == 2){
			return true;
		}
		return false;
	}

	public static void main(String[] args) {

		System.out.println(prime(200));
		System.out.println(prime(2));
		System.out.println(prime(7));
	}

}