package day08;
//passing array as an argument
public class Demo1 {

	public static void showArray(int arr[]){
		for(int i = 0; i < arr.length; i++){
			System.out.print(arr[i] + " ");
		}
	}

	public static void main(String[] args) {

		int arr[]= {10, 30, 50, 70, 90};
		showArray(arr);


	}

}
