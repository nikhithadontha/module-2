package day08;

public class Demo3 {
	//Linear Search
	public static int searchArray(int arr[], int num){
		for(int i = 0; i < arr.length-1;i++){
			if(num == arr[i]){
				return i;
			}
		}
		return -1;
	}

	public static void main(String[] args) {
		int arr[] = {30, 10, 50, 20, 40};

		System.out.println(searchArray(arr, 99));
		System.out.println(searchArray(arr, 10));
		System.out.println(searchArray(arr, 20));

	}

}
