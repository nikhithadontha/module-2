package day09;

public class Demo9 {

	public static int findSecondLargest(int arr[]){

		int temp;
		int l = arr.length;
		for(int i = 0; i < l; i++){
			for(int j = 0; j < l; j++){
				if(arr[i] > arr[j]){
					temp = arr[i];
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
		return arr[1];
	}

	public static void main(String[] args) {
		int arr1[] = {10 , 20, 30, 40, 50};
		int arr2[] = {83, 96, 125, 183, 458, 329};
		int arr3[] = {430, 480, 350};
		int arr4[] = {480, 350};


		System.out.println(findSecondLargest(arr1));
		System.out.println(findSecondLargest(arr2));
		System.out.println(findSecondLargest(arr3));
		System.out.println(findSecondLargest(arr4));




	}

}
