package javaproject;
import java.util.Scanner;

public class Multiplication_Table {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.print("Enter the number: ");
		int n = scanner.nextInt();


		for (int i = 1; i <= n; i++) {
			System.out.println("Multiplication Table of " + i + ":");

			for (int j = 1; j <= 10; j++) {
				System.out.println(i + " * " + j + " = " + (i * j));
			}
			System.out.println();
		}

		scanner.close();
	}
}
